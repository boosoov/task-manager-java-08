package com.rencredit.jschool.boruak.taskmanager.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String ARG_HELP = "-h";

    String ARG_VERSION = "-v";

    String ARG_ABOUT = "-a";

    String ARG_EXIT = "-e";

    String ARG_INFO = "-i";

}
