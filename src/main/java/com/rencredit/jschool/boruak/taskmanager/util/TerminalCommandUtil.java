package com.rencredit.jschool.boruak.taskmanager.util;

import com.rencredit.jschool.boruak.taskmanager.constant.TerminalConst;

public interface TerminalCommandUtil {

    static String convertARGToCMD(final String arg) {
        if (arg == null || arg.isEmpty()) return arg;
        switch (arg) {
            case TerminalConst.ARG_HELP: return TerminalConst.CMD_HELP;
            case TerminalConst.ARG_ABOUT: return TerminalConst.CMD_ABOUT;
            case TerminalConst.ARG_VERSION: return TerminalConst.CMD_VERSION;
            case TerminalConst.ARG_INFO: return TerminalConst.CMD_INFO;
            case TerminalConst.ARG_EXIT: return TerminalConst.CMD_EXIT;
            default: return arg;
        }
    }

}
