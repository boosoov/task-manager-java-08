package com.rencredit.jschool.boruak.taskmanager;

import com.rencredit.jschool.boruak.taskmanager.model.Terminal;

public class Application {

    public static void main(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER ** \n");
        Terminal.parseArgs(args);
        while(true) {
            Terminal.processCommand();
        }
    }

}
