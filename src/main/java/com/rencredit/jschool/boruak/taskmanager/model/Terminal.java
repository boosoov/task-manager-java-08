package com.rencredit.jschool.boruak.taskmanager.model;

import com.rencredit.jschool.boruak.taskmanager.constant.TerminalConst;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalCommandUtil;
import com.rencredit.jschool.boruak.taskmanager.util.NumberUtil;

import java.util.Scanner;

public class Terminal {

    public static void processCommand() {
        final Scanner scanner = new Scanner(System.in);
        String[] commands;
        while (true) {
            System.out.print("Enter command: ");
            commands = scanner.nextLine().split("\\s+");
            System.out.println();
            Terminal.parseArgs(commands);
        }
    }

    public static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        for (String arg : args) {
            processArg(TerminalCommandUtil.convertARGToCMD(arg));
            System.out.println();
        }
    }

    private static void processArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_INFO:
                showInfo();
                break;
            case TerminalConst.CMD_EXIT:
                System.exit((0));
                break;
            default:
                showUnknownCommand(arg);
        }
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Boruak Sergey");
        System.out.println("E-MAIL: boosoov@gmail.com");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalCommand.ABOUT);
        System.out.println(TerminalCommand.HELP);
        System.out.println(TerminalCommand.INFO);
        System.out.println(TerminalCommand.VERSION);
        System.out.println(TerminalCommand.EXIT);

    }

    private static void showUnknownCommand(final String arg) {
        System.out.println("Unknown command: " + arg);
    }

    private static void showInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
